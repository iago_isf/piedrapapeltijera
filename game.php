<?php
    if ($_SERVER['REQUEST_METHOD'] === 'POST')
    {
        if(isset($_POST['logout'])){
            header('Location: index.php');
            die();
        }   
    }

    if(isset($_GET['name']) && $_GET['name'] != '') {
        $name = $_GET['name'];
    }
    else {
        die("Le falta el nombre del parámetro");
    }

    $choice = isset($_POST['select']) ? $_POST['select'] : 0; 

    function check($computer, $human){
        if ($computer == 'piedra'  && $human == 'piedra' ||
            $computer == 'papel'   && $human == 'papel' ||
            $computer == 'tijeras' && $human == 'tijeras'){
            return "empate";
        }
        else if($computer == 'piedra'  && $human == 'tijeras' ||
                $computer == 'papel'   && $human == 'piedra' ||
                $computer == 'tijeras' && $human == 'papel'){
            return "pierdes";
        }
        else if($computer == 'piedra'  && $human == 'papel' ||
                $computer == 'papel'   && $human == 'tijeras' ||
                $computer == 'tijeras' && $human == 'piedra'){
            return "ganas";
        }
        else {
            return 'Algo salió mal';
        }
    }

    function play($value){
        $names = ['piedra', 'papel', 'tijeras'];
        $comp = random_int(0,2);

        switch ($value){
            case 0:
                echo 'Selecciona una opción del menú desplegable.'; 
                break;
            case 1:
                echo "Tu jugada = $names[0]; Ordenador = $names[$comp]; Resultado = ".check($names[$comp], $names[0]);
                break;
            case 2:
                echo "Tu jugada = $names[1]; Ordenador = $names[$comp]; Resultado = ".check($names[$comp], $names[1]);
                break;
            case 3:
                echo "Tu jugada = $names[2]; Ordenador = $names[$comp]; Resultado = ".check($names[$comp], $names[2]);
                break;
            case 4:
                for($c=0;$c<3;$c++) {
                    for($h=0;$h<3;$h++) {
                        echo 'Humano='.$names[$h].' Ordenador='.$names[$c].' Result='.check($names[$c], $names[$h])."\n";
                    }
                }
                break;
        }
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Iago Senín</title>
</head>
<body>
    <h1>Piedra, Papel o Tijera</h1>
    <p>Bienvenido <?= $name ?></p>

    <form action="#" method="POST">
        <select name="select" id="select">
            <option value="0">-- Selecciona --</option>
            <option value="1">Piedra</option>
            <option value="2">Papel</option>
            <option value="3">Tijera</option>
            <option value="4">Test</option>
        </select>
        <input type="submit" name="jugar" value="Jugar">
        <input type="submit" name="logout" value="Logout">
    </form>
    <br>
    <textarea name="" id="" cols="60" rows="10">
<?php play($choice); ?>
    </textarea>
</body>
</html>