<?php
    if ($_SERVER['REQUEST_METHOD'] === 'POST'){
        $salt = 'XyZzy12*_';
        $stored_hash = '1a52e17fa899cf40fb04cfc42e6352f1';

        $name = isset($_POST['name']) ? $_POST['name'] : '';
        $pass = isset($_POST['pass']) ? $salt.$_POST['pass'] : '';

        $text = '';

        if($name == '' && $pass == '' || $name == '' && $pass == $salt){
            $text = '<p>Se requiere un nombre de usuario y una contraseña para acceder</p>';
        }
        else if (hash('md5', $pass) != $stored_hash) {
            $text = '<p>Contraseña incorrecta</p>';
        } 
        else {
            header("Location: game.php?name=".urlencode($_POST['name']));
            die();
        } 
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Iago Senín</title>
</head>
<body>
    <h1>Inicie sesión</h1>
    <?php if(isset($text)) echo $text; ?>
    <form action="" method="POST">
        <p>Nombre: <input type="text" name="name" id="name"></p>
        <p>Contraseña: <input type="password" name="pass" id="pass"></p>
        <p><input type="submit" value="Iniciar sesión"></p>
    </form>
</body>
</html>